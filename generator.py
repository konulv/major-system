#import twl
from itertools import product # https://devdocs.io/python~3.10/library/itertools#itertools.product
from random import randint, choice

MAJOR_W = [["s", "z"],
         ["t", "d"],
         ["n"],
         ["m"],
         ["r"],
         ["l"],
         ["g", "j", "sh", "ch", "zh"],
         ["k", "c"],
         ["f", "v"],
         ["b", "p"]]
		 
MAJOR_P = [["s", "z"],
         ["t", "d", "θ", "ð"],
         ["n"],
         ["m"],
         ["r"],
         ["l"],
         ["tʃ", "dʒ", "ʃ", "ʒ", "zh"],
         ["k", "ɡ"],
         ["f", "v"],
         ["b", "p"],
		 ["ŋ"]]
		 
PHO_WORDS = []
with open("pho_words.txt", "r", encoding="utf8") as file:
	PHO_WORDS = file.readline().split(",")

NOR_WORDS = []
with open("nor_words.txt", "r") as file:
	NOR_WORDS = file.readline().split(",")


# num has to be a string to begin with
# generates semi-random letters
# mainly for manual practicing of converting letters back to numbers
# random=True more for guessing game/practice
# random=False gives a list of possibilities for each number, in order
# mostly used internally
def numToLetters(num, random=True):
    if random:
        letters = ""
        for i in num:
            letters += choice(MAJOR_W[int(i)])
        return letters
    else:
        lst = []
        for i in num:
            lst.append(MAJOR_W[int(i)])
        return lst


def lettersToNum(letters):
    num = ""
    i = 0
    while i < len(letters):
        # edge case, 6 has 2 letter variants sh,ch,zh
        # 0 has s,z letters
        # gotta find out which one it is, if its followed by "h" it has to be 6
        # 0 otherwise
        if letters[i] in ["s", "z"]:
            try:
                if letters[i+1] == "h":
                    i += 1
                    num += "6"
                else:
                    num += "0"
            except IndexError:
                num += "0"
        else:
            for index, j in enumerate(MAJOR_W):
                if letters[i] in j:
                    num += str(index)
        i += 1
    return num


def generateNumber(length):
    num = ""
    for i in range(length):
        num += str(randint(0, 9))
    return num

# given a number
# give a list of words that could fit
# this needs to be rethinked, and i need to create a new list
def suggestWords(num):
    lst = numToLetters(str(num), False)
    print(lst)
    choices = list(product(*lst))
    # now its just sorting out and string matching words
    return choices

