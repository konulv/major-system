import unittest
import generator

class TestStringMethods(unittest.TestCase):

    def test_number_to_words(self):
        self.assertEqual(generator.numToLetters("234"), "nmr")
        self.assertEqual(generator.numToLetters("012", False), [["s", "z"], ["t", "d"], ["n"]])

    def test_words_to_number(self):
        self.assertEqual(generator.lettersToNum("tazer"), "104")
        self.assertEqual(generator.lettersToNum("sushi"), "06")
        self.assertEqual(generator.lettersToNum("bpjm"), "9963")

    def test_insert_new_rules(self):
        return

    def test_add_new_suggestion(self):
        return

    def test_number_to_suggestion(self):
        return

if __name__ == '__main__':
    unittest.main()